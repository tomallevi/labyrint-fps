#ifndef __LEVEL_H__
#define __LEVEL_H__

#include <nodePath.h>

void load_level(string name);
void level_hit(const Event* ev, void* data);

#endif
