#include "level.h"
#include "globalVariable.h"

#include "tasks.h"

#include <genericAsyncTask.h>

#include <collisionNode.h>
#include <collisionSphere.h>
#include <collisionPolygon.h>

#include <collisionHandlerQueue.h>
#include <nodePathCollection.h>

void add_ball_plane(NodePath plane);
void setUpHprs();

void load_level(string level_name) {
	// load level
	level = framework.get_window(0)->load_model(framework.get_models(), "egg_models/"+level_name);
	level.reparent_to(framework.get_window(0)->get_render());
	level.set_scale(SCALE, SCALE, SCALE);
	level.set_tag("who", "level");
	
	laser_queue = new CollisionHandlerQueue();
	
	NodePathCollection collection = level.find_all_matches("**/ballPlane*");
	for(int i=0; i < collection.size(); i++)
		add_ball_plane(collection[i]);
	
	NodePath end = level.find("**/end_point");
	CollisionNode* cnode = new CollisionNode("end");
	cnode->add_solid(new CollisionSphere(0, 0, 0, 3));
	cnode->set_into_collide_mask(BitMask32::bit(24));
	end = end.attach_new_node(cnode);
	#ifdef DEBUG
	end.show();
	#endif
	end.set_tag("who", "end");
	
	setUpHprs();
	
	laser_task = new GenericAsyncTask("laser update", &laser_update, (void*)NULL);
	laser_task->set_delay(0.02);
	taskMgr->add(laser_task);
}


void add_ball_plane(NodePath plane) {
	// add plane
	CollisionNode* cnode = new CollisionNode("ballPlane");
	cnode->add_solid(new CollisionPolygon(LVecBase3f(1, 1, 0), LVecBase3f(-1, 1, 0), LVecBase3f(-1, -1, 0), LVecBase3f(1, -1, 0)));
	cnode->set_into_collide_mask(BitMask32::bit(31));
	NodePath node = plane.attach_new_node(cnode);
	node.set_tag("who", "ballPlane");
	#ifdef DEBUG
	node.show();
	#endif
}

// bullets hits level
// remove bullet
void level_hit(const Event* ev, void* data) {
	cout << "level hit" << endl;
	CollisionEntry* entry = (CollisionEntry*) ev->get_parameter(0).get_ptr();
	bullets.erase(entry->get_from_node_path().get_parent());
	entry->get_from_node_path().get_parent().remove_node();
}


void setUpHprs() {
	// this is the orientation of laser beam for each balls
	hprs.push_back(vector<int>());
	hprs[0].push_back(0);
	hprs[0].push_back(0);
	hprs[0].push_back(0);
	
	hprs.push_back(vector<int>());
	hprs[1].push_back(0);
	hprs[1].push_back(90);
	hprs[1].push_back(0);
	hprs.push_back(vector<int>());
	hprs[2].push_back(0);
	hprs[2].push_back(180);
	hprs[2].push_back(0);
	hprs.push_back(vector<int>());
	hprs[3].push_back(0);
	hprs[3].push_back(270);
	hprs[3].push_back(0);
	hprs.push_back(vector<int>());
	hprs[4].push_back(0);
	hprs[4].push_back(0);
	hprs[4].push_back(90);
	hprs.push_back(vector<int>());
	hprs[5].push_back(0);
	hprs[5].push_back(0);
	hprs[5].push_back(270);
}

