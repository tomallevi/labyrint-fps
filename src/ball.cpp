#include "ball.h"
#include "globalVariable.h"

#include <nodePathCollection.h>

#include <collisionEntry.h>

#include <cMetaInterval.h>
#include <cLerpNodePathInterval.h>

#include <collisionSphere.h>
#include <collisionRay.h>


void add_ball(string name);

void request_to_add_ball(const Event* ev, void* data) {
	// player has touched a ballPlane
	CollisionEntry* entry = (CollisionEntry*) ev->get_parameter(0).get_ptr();
	string name = entry->get_into_node_path().get_parent().get_name();
	name = name.substr(9);
	name = name.substr(0, name.find("."));
	cout << name;
	// if isn't already loaded
	if (!is_in_set(loaded_ball_rays, name))
		add_ball(name);
	else
		cout << " already loaded" << endl;
}


// add ball
void add_ball(string name) {
	// load model
	NodePath ball = NodePath("ball");
	models["ball"].copy_to(ball);
	ball.reparent_to(framework.get_window(0)->get_render());
	ball.set_pos(level.find("**/"+name).get_pos()*SCALE);
	
	ball.set_tag("who", "ball");
	ball.set_tag("ball", name);
	// add interval
	CLerpNodePathInterval* interval = new CLerpNodePathInterval(name+"interval", 10.0, CLerpInterval::BT_no_blend, true, false, ball, NodePath());
	// TODO: does it make random ?
	interval->set_start_hpr(LVecBase3f(0, 0, 0));
	interval->set_end_hpr(LVecBase3f(180, 90, 180));
	// sequence
	CMetaInterval* sequence = new CMetaInterval(name+"sequence");
	sequence->add_c_interval(interval, 0, CMetaInterval::RS_previous_end);
	sequence->loop();
	
	CollisionNode* cnode;
	NodePath tube, node;
	// collision ball node
	cnode = new CollisionNode("ball");
	cnode->add_solid(new CollisionSphere(0, 0, 0, 1.2));
	cnode->set_from_collide_mask(BitMask32::all_off());
	cnode->set_into_collide_mask(BitMask32::bit(27));
	node = ball.attach_new_node(cnode);
	#ifdef DEBUG
	node.show();
	#endif
	
	BallFeature* feature = new BallFeature();
	feature->life = 3;
	feature->damage = 1;
	features[name] = feature;
	
	ball.set_color(color_ball[feature->life]);
	
	// load laser beam
	int i=0;
	char str[2];
	for (vector<vector<int> >::iterator ii = hprs.begin(); ii != hprs.end(); ii++, i++) {
		tube = NodePath("tube");
		models["tube"].copy_to(tube);
		tube.reparent_to(ball);
		tube.set_hpr((*ii)[0], (*ii)[1], (*ii)[2]);
		tube.set_collide_mask(BitMask32::all_off());
		
		cnode = new CollisionNode("beam");
		cnode->add_solid(new CollisionRay(0, 0, 0, 0, 0, 1));
		cnode->set_from_collide_mask(BitMask32::bit(1) | BitMask32::bit(30));
		cnode->set_into_collide_mask(BitMask32::all_off());
		node = tube.attach_new_node(cnode);
		#ifdef DEBUG
		node.show();
		#endif
		level_traverser->add_collider(node, laser_queue);
		
		sprintf(str, "%d", i);
		node.set_tag("who", name+str);
		loaded_ball_rays.insert(name+str);
	}
	
	cout << " loaded" << endl;
}

// bullets hits a ball
// remove bullet and ball
void ball_hit(const Event* ev, void* data) {
	cout << "ball hit" << endl;
	CollisionEntry* entry = (CollisionEntry*) ev->get_parameter(0).get_ptr();
	entry->get_from_node_path().get_parent().remove_node();
	cout << "bullet removed" << endl;
	
	string name = entry->get_into_node_path().get_parent().get_tag("ball");
	cout << name << " " << features[name]->life << endl;
	features[name]->life --;
	
	CLerpNodePathInterval* interval = new CLerpNodePathInterval(name+"ColorInterval", 0.5, CLerpInterval::BT_no_blend, true, false, entry->get_into_node_path().get_parent(), NodePath());
	interval->set_start_color(color_ball[features[name]->life +1]);
	interval->set_end_color(color_ball[features[name]->life]);
	CMetaInterval* sequence = new CMetaInterval(name+"ColorSequence");
	sequence->add_c_interval(interval, 0, CMetaInterval::RS_previous_end);
	sequence->start();
	if (features[name]->life != 0) {
		return;
	}
	NodePathCollection collection = level.find_all_matches("**/ballPlane"+name+"*");
	for (int i =0; i< collection.size(); i++)
		collection[i].remove_node();
	bullets.erase(entry->get_from_node_path().get_parent());
	
	entry->get_into_node_path().get_parent().remove_node();
	cout << "ball removed" << endl;
	for (set<string>::iterator ii = loaded_ball_rays.begin(); ii!= loaded_ball_rays.end(); ii++)
		if (ii->find(name) != string::npos) loaded_ball_rays.erase(ii);
}
