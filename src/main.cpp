
#include <collisionNode.h>
#include <collisionSphere.h>
#include <pointLight.h>

#include <string>
using std::string;

#include <load_prc_file.h>

#include "globalVariable.h"
#include "level.h"
#include "tasks.h"
#include "ball.h"
#include "player.h"

// called when a key is pressed
void setKeyTrue(const Event* ev, void* data) {
	keys[(int) data] = true;
}

// called when a key is released
void setKeyFalse(const Event* ev, void* data) {
	keys[(int) data] = false;
}

// called when is the time to exit
void fps_exit(const Event* ev, void* data) {
	cout << "bullets alive " << bullets.size() << endl;
	cout << "ball rays alive " << loaded_ball_rays.size() << endl;
	exit(0);
}

void player_start_firing(const Event* ev, void* data) {
	// add the player fire's task
	player_fire(NULL, NULL);
	fire_task = new GenericAsyncTask("player fire", &player_fire, (void*) NULL);
	fire_task->set_delay(0.2);
	taskMgr->add(fire_task);
}

void player_stop_firing(const Event* ev, void* data) {
	// remove the player fire's task
	taskMgr->remove(fire_task);
}

void setUpKeyboard(WindowFramework* window) {
	// enable input from keyboard
	window->enable_keyboard();
	
	framework.define_key("w", "w-down", &setKeyTrue, (void*)FORWARD);
	framework.define_key("w-up", "w-up", &setKeyFalse, (void*)FORWARD);
	framework.define_key("a", "a-down", &setKeyTrue, (void*)LEFT);
	framework.define_key("a-up", "a-up", &setKeyFalse, (void*)LEFT);
	framework.define_key("s", "s-down", &setKeyTrue, (void*)BACK);
	framework.define_key("s-up", "s-up", &setKeyFalse, (void*)BACK);
	framework.define_key("d", "d-down", &setKeyTrue, (void*)RIGHT);
	framework.define_key("d-up", "d-up", &setKeyFalse, (void*)RIGHT);
	framework.define_key("q", "q-down", &setKeyTrue, (void*)UP);
	framework.define_key("q-up", "q-up", &setKeyFalse, (void*)UP);
	framework.define_key("e", "e-down", &setKeyTrue, (void*)DOWN);
	framework.define_key("e-up", "e-up", &setKeyFalse, (void*)DOWN);
	framework.define_key("shift", "shift-down", &setKeyTrue, (void*)RUN);
	framework.define_key("shift-up", "shift-up", &setKeyFalse, (void*)RUN);
	// if shift if pressed
	framework.define_key("shift-w", "w-down", &setKeyTrue, (void*)FORWARD);
	framework.define_key("shift-w-up", "w-up", &setKeyFalse, (void*)FORWARD);
	framework.define_key("shift-a", "a-down", &setKeyTrue, (void*)LEFT);
	framework.define_key("shift-a-up", "a-up", &setKeyFalse, (void*)LEFT);
	framework.define_key("shift-s", "s-down", &setKeyTrue, (void*)BACK);
	framework.define_key("shift-s-up", "s-up", &setKeyFalse, (void*)BACK);
	framework.define_key("shift-d", "d-down", &setKeyTrue, (void*)RIGHT);
	framework.define_key("shift-d-up", "d-up", &setKeyFalse, (void*)RIGHT);
	framework.define_key("shift-q", "q-down", &setKeyTrue, (void*)UP);
	framework.define_key("shift-q-up", "q-up", &setKeyFalse, (void*)UP);
	framework.define_key("shift-e", "e-down", &setKeyTrue, (void*)DOWN);
	framework.define_key("shift-e-up", "e-up", &setKeyFalse, (void*)DOWN);
	
	framework.define_key("escape", "exit", &fps_exit, 0);
	
	framework.define_key("mouse1", "start-firing", &player_start_firing, (void*)NULL);
	framework.define_key("mouse1-up", "stop-fining", &player_stop_firing, (void*)NULL);
	framework.define_key("shift-mouse1", "start-firing", &player_start_firing, (void*)NULL);
	framework.define_key("shift-mouse1-up", "stop-fining", &player_stop_firing, (void*)NULL);
	
}

void player_lose() {
	cout << "player lose" << endl;
	fps_exit(NULL, NULL);
}

// player has won
void player_win(const Event* ev, void* data) {
	cout << "player win" << endl;
	fps_exit(NULL, NULL);
}


extern "C" {
int run() {
	char **argv;
	int argc = 0;
	load_prc_file_data("", "show-frame-rate-meter 1");
	
	framework.open_framework(argc, argv);
	framework.set_window_title("Hello World!");
	WindowFramework *window = framework.open_window();
	
	gw = window->get_graphics_window();
	
	models["ball"] = window->load_model(framework.get_models(), "egg_models/ball");
	models["bullet"] = window->load_model(framework.get_models(), "egg_models/bullet");
	models["tube"] = window->load_model(framework.get_models(), "egg_models/tube");
	
	NodePath mirino = window->load_model(framework.get_models(), "textures/viewfinder.png");
	mirino.reparent_to(window->get_render_2d());
	mirino.set_scale(0.007, 1, 0.007);
	
	level_traverser = new CollisionTraverser();
	
	load_level("level2");
	setUpPlayer();
	
	setUpKeyboard(window);

	framework.main_loop();
  
	framework.close_framework();
	return 0;
}
}
