#ifndef __BALL_H__
#define __BALL_H__

#include <event.h>

#include <map>
using std::map;

void request_to_add_ball(const Event* ev, void* data);
void ball_hit(const Event* ev, void* data);

#endif
