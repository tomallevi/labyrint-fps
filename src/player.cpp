#include "player.h"
#include "globalVariable.h"
#include "ball.h"
#include "level.h"
#include "tasks.h"

#include <pointLight.h>
#include <collisionSphere.h>
#include <collisionNode.h>

int player_life = BEGIN_PLAYER_LIFE;

void player_hit() {
	cout << "player hit" << endl;
	player_life --;
	char str[2];
	sprintf(str, "%d", player_life);
	((TextNode*)player_life_node.node())->set_text(str);
	if (player_life == 0) 
		player_lose();
}


void setUpPlayer() {
	// player node setup
	player_node = framework.get_window(0)->get_render().attach_new_node("player");
	// the camera is attached to player node (first person version)
	framework.get_window(0)->get_camera_group().reparent_to(player_node);
	
	player_node.set_pos(level.find("**/start_point").get_pos()*SCALE);
	// task to move player
	GenericAsyncTask* task = new GenericAsyncTask("move player", &move_player, (void*)NULL);
	task->set_delay(0.005);
	taskMgr->add(task);
	// light
	PointLight* light = new PointLight("player light");
	NodePath nlight = player_node.attach_new_node(light);
	framework.get_window(0)->get_render().set_light(nlight);
	
	// player vs walls
	CollisionHandlerPusher* player_pusher = new CollisionHandlerPusher();
	player_pusher->set_horizontal(false);
	CollisionNode* cnode = new CollisionNode("playerWallSphere");
	cnode->add_solid(new CollisionSphere(0, 0, 0, 2.0));
	cnode->set_from_collide_mask(BitMask32::bit(1));
	cnode->set_into_collide_mask(BitMask32::all_off());
	NodePath node = player_node.attach_new_node(cnode);
	player_pusher->add_collider(node, player_node);
	level_traverser->add_collider(node, player_pusher);
	// player vs other things
	player_eventer = new CollisionHandlerEvent();
	player_eventer->add_in_pattern("%(who)ft-into-%(who)it");
	cnode = new CollisionNode("playerSizeSphere");
	cnode->add_solid(new CollisionSphere(0, 0, 0, 1));
	cnode->set_from_collide_mask(BitMask32::bit(31) | BitMask32::bit(24));
	cnode->set_into_collide_mask(BitMask32::bit(30));
	node = player_node.attach_new_node(cnode);
	node.set_tag("who", "player");
	level_traverser->add_collider(node, player_eventer);
	// player vs ballPlane
	framework.define_key("player-into-ballPlane", "add ball", &request_to_add_ball, (void*)NULL);
	// player vs end
	framework.define_key("player-into-end", "end", &player_win, (void*)NULL);
	
	// other events
	framework.define_key("bullet-into-ball", "ball hit", &ball_hit, (void*)NULL);
	framework.define_key("bullet-into-level", "level hit", &level_hit, (void*)NULL);
	// bullet task
	task = new GenericAsyncTask("move bullet", &move_bullet, (void*)NULL);
	task->set_delay(0.01);
	taskMgr->add(task);
	
	TextNode* text = new TextNode("player life");
	char str[2];
	sprintf(str, "%d", BEGIN_PLAYER_LIFE);
	text->set_text(str);
	player_life_node = framework.get_window(0)->get_aspect_2d().attach_new_node(text);
	player_life_node.set_scale(0.07);
	player_life_node.set_pos(0.9, 1, -0.9);
}
