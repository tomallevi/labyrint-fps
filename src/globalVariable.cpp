#include "globalVariable.h"

map<int, LVecBase4> get_color_ball();

PandaFramework framework;
NodePath player_node;
NodePath level;
NodePath player_life_node;
AsyncTaskManager* taskMgr  = AsyncTaskManager::get_global_ptr();
GraphicsWindow* gw;

set<string> loaded_ball_rays;
set<NodePath> bullets;
vector<vector<int> > hprs;
map<string, BallFeature*> features;
map<string, NodePath> models;
map<int, LVecBase4> color_ball = get_color_ball();

CollisionTraverser* level_traverser;
CollisionHandlerQueue* laser_queue;
CollisionHandlerEvent* player_eventer;
GenericAsyncTask* laser_task;
GenericAsyncTask* fire_task;

bool is_in_set(set<string> st, string name) {
	for (set<string>::iterator ii = st.begin(); ii!= st.end(); ii++)
		if (ii->find(name) != string::npos) return true;
	return false;
}

bool keys[] = {false, false, false, false, false, false, false};

map<int, LVecBase4> get_color_ball() {
	map<int, LVecBase4> colors;
	colors[0] = LVecBase4(0, 0, 0, 0);
	colors[1] = LVecBase4(0, 0, 1, 0.5);
	colors[2] = LVecBase4(0, 1, 1, 1);
	colors[3] = LVecBase4(1, 1, 1, 1);
	
	return colors;
}
