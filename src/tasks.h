#ifndef __TASKS_H__
#define __TASKS_H__

#include <genericAsyncTask.h>
#include <asyncTaskManager.h>

AsyncTask::DoneStatus laser_update(GenericAsyncTask* task, void* data);
AsyncTask::DoneStatus move_player(GenericAsyncTask* task, void* data);
AsyncTask::DoneStatus move_bullet(GenericAsyncTask* task, void* data);
AsyncTask::DoneStatus player_fire(GenericAsyncTask* task, void* data);

#endif
