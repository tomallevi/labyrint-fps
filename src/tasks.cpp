#include "tasks.h"
#include "globalVariable.h"
#include "bullet.h"
#include "player.h"

#include <cIntervalManager.h>
#include <clockObject.h>

ClockObject* fps_clock = ClockObject::get_global_clock();
double last_hit = 0;

// laser task
AsyncTask::DoneStatus laser_update(GenericAsyncTask* task, void* data) {
	laser_queue->sort_entries();
	static set<string> laser_beam;
	static CollisionEntry* entry;
	static int i;
	static string name;
	laser_beam = set<string>(loaded_ball_rays);
	i=0;
	while (!laser_beam.empty() && i < laser_queue->get_num_entries()) {
		entry = laser_queue->get_entry(i++);
		name = entry->get_from_node_path().get_tag("who");
		if (entry->get_into_node_path().get_tag("who") == "player" && last_hit + 0.5 < fps_clock->get_long_time()) {
			player_hit();
			last_hit = fps_clock->get_long_time();
		}
		if (!is_in_set(laser_beam, name))
			continue;
		laser_beam.erase(name);
		entry->get_from_node_path().get_parent().set_scale(1, 1, entry->get_surface_point(entry->get_from_node_path().get_parent().get_parent()).length() +0.1);
	}
	return AsyncTask::DS_cont;
}

AsyncTask::DoneStatus move_player(GenericAsyncTask* task, void* data) {
	// change hpr of player node and move the pointer ad the center
  	int x = gw->get_pointer(0).get_x() - gw->get_properties().get_x_size() / 2;
  	int y = gw->get_pointer(0).get_y() - gw->get_properties().get_y_size() / 2;
	if (gw->move_pointer(0, gw->get_properties().get_x_size()/2, gw->get_properties().get_y_size()/2)) {
		player_node.set_h(player_node.get_h() - (x * VEL_ROT));
		player_node.set_p(player_node.get_p() - (y* VEL_ROT));
	}
	// calculate te movement
	LVecBase3f move(0, 0, 0);
	if (keys[FORWARD])
		move += player_node.get_mat().get_row3(1);
	if (keys[BACK])
		move -= player_node.get_mat().get_row3(1);
	if (keys[RIGHT])
		move += player_node.get_mat().get_row3(0);
	if (keys[LEFT])
		move -= player_node.get_mat().get_row3(0);
	if (keys[UP])
		move += player_node.get_mat().get_row3(2);
	if (keys[DOWN])
		move -= player_node.get_mat().get_row3(2);
	move.normalize();
	player_node.set_pos(player_node.get_pos() + move*0.01*(keys[RUN] ? VEL_RUN : VEL_GO));
	// traverse through render
	level_traverser->traverse(framework.get_window(0)->get_render());
	// animating system step
	CIntervalManager::get_global_ptr()->step();
	
	return AsyncTask::DS_again;
}


// move the bullets
AsyncTask::DoneStatus move_bullet(GenericAsyncTask* task, void* data) {
	for (set<NodePath>::iterator ii = bullets.begin(); ii != bullets.end(); ii++) 
		((NodePath)(*ii)).set_fluid_pos(*ii, 0, 1.5, 0);	
	return AsyncTask::DS_again;
}

AsyncTask::DoneStatus player_fire(GenericAsyncTask* task, void* data) {
	create_bullet();
	return AsyncTask::DS_again;
}
