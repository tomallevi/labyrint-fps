#include "bullet.h"
#include "globalVariable.h"

#include <nodePath.h>
#include <collisionNode.h>
#include <collisionSegment.h>

void create_bullet() {
	// load the bullet's model
	NodePath bullet = NodePath("bullet");
	models["bullet"].copy_to(bullet);
	bullet.reparent_to(level);
	bullet.set_pos(player_node.get_pos(level));
	bullet.set_scale(1/SCALE, 1/SCALE, 1/SCALE);
	bullet.set_hpr(player_node.get_hpr());
	
	// add collider
	CollisionNode* cnode = new CollisionNode("bullet");
	cnode->add_solid(new CollisionSegment(0, -1, 0, 0, 1.1, 0));
	cnode->set_from_collide_mask(BitMask32::bit(1) | BitMask32::bit(27));
	cnode->set_into_collide_mask(BitMask32::all_off());
	NodePath node = bullet.attach_new_node(cnode);
	level_traverser->add_collider(node, player_eventer);
	#ifdef DEBUG
	node.show();
	#endif
	
	node.set_tag("who", "bullet");
	
	bullets.insert(bullet);
}
