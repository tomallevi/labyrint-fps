#ifndef __GLOBAL_VARIABLE_H__
#define __GLOBAL_VARIABLE_H__

#include <pandaFramework.h>
#include <pandaFramework.h>
#include <pandaSystem.h>
#include <genericAsyncTask.h>

#include <collisionTraverser.h>
#include <collisionHandlerPusher.h>
#include <collisionHandlerEvent.h>
#include <collisionHandlerQueue.h>

#include <set>
using std::set;
#include <vector>
using std::vector;

#define SCALE 7.0

#define VEL_ROT 0.2
#define VEL_GO 5
#define VEL_RUN 20

#define BEGIN_PLAYER_LIFE 3

typedef struct _BallFeature {
	int life;
	int damage;
} BallFeature;


extern PandaFramework framework;

extern AsyncTaskManager* taskMgr;
extern GraphicsWindow* gw;
extern NodePath player_node;
extern NodePath level;
extern NodePath player_life_node;

extern set<string> loaded_ball_rays;
extern set<NodePath> bullets;
extern vector<vector<int> > hprs;
extern map<string, BallFeature*> features;
extern map<string, NodePath> models;
extern map<int, LVecBase4> color_ball;

extern CollisionTraverser* level_traverser;
extern CollisionHandlerQueue* laser_queue;
extern CollisionHandlerEvent* player_eventer;
extern GenericAsyncTask* laser_task;
extern GenericAsyncTask* fire_task;

bool is_in_set(set<string> st, string name);

typedef enum {
	FORWARD, BACK, UP, DOWN, LEFT, RIGHT, RUN
} keys_name;

extern bool keys[];

extern void player_lose();
extern void player_win(const Event* ev, void* data);
#endif
